function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
    
        reader.onload = function (e) {
            $('#faceimg')
                .attr('src', e.target.result)
                .width(360)
                .height(360);
        };
    
        reader.readAsDataURL(input.files[0]);
    }
    }
    
    function changeValue(obj){
        $('#rangevalue')[0].innerHTML=obj.value;
    }