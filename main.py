import os
from app import app
from flask import Flask, flash, request, redirect, render_template, Response
from werkzeug.utils import secure_filename
from flask import make_response
from datetime import datetime
import sys
#import dlib
import face_recognition
import os
import postgresql
from PIL import Image, ImageDraw
from werkzeug.http import http_date
import time
import base64
import numpy as np
from lib.camera import Camera

camera = Camera(flip=True)

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])

def pagination(current,total):
    list=[]
    limit=3
    upper=lower = min(current,total)
    i=1
    while(i<limit and i < total):
        if lower>1:
            lower-=1
            i+=1
        if i < limit and upper < total:
            upper+=1
            i+=1
    for b in range(lower,upper+1):
        list.append(b)
    return list

def allowed_file(filename):
	return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def get_base64_encoded_image(image_path):
    with open(image_path, "rb") as img_file:
        return base64.b64encode(img_file.read()).decode('utf-8')

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/result')
def result_form():
	return render_template('result.html',title="Recognition Result")

@app.route('/recognition')
def recognition_form():
	return render_template('recognition.html',title="Face Recognition Test")

@app.route('/recognition',methods=['POST'])
def recognition():	
	threshold=request.form["threshold"]		
	file=request.files['file']	
	filename=secure_filename(file.filename)
	db = postgresql.open(app.config['DB_CONN'])
	file.save(os.path.join(app.config['UPLOAD_FOLDER'],filename))
	test_image = face_recognition.load_image_file(os.path.join(app.config['UPLOAD_FOLDER'],filename))
	face_locations = face_recognition.face_locations(test_image)
	face_encodings = face_recognition.face_encodings(test_image, face_locations)
	pil_image = Image.fromarray(test_image)
	draw = ImageDraw.Draw(pil_image)
	start_time = time.time()
	for (top, right, bottom, left), face_encoding in zip(face_locations, face_encodings):
	    name = "Unknown"   
	    if len(face_encoding) > 0:
        	query = "SELECT name FROM encodings WHERE sqrt(power(CUBE(array[{}]) <-> vector, 2)) <= {} ".format(           	','.join(str(s) for s in face_encoding[0:128]),
	           threshold)+\
				   "ORDER BY sqrt(power(CUBE(array[{}]) <-> vector, 2)) ASC LIMIT 1".format(','.join(str(s) for s in face_encoding[0:128]))
	        q=db.query(query)
        	draw.rectangle(((left, top), (right, bottom)), outline=(255, 0, 0))
        
	        if(len(q)>0):
        	    name=q[0][0]
	        text_width, text_height = draw.textsize(name)
	        draw.rectangle(((left, bottom - text_height + 10), (right, bottom+10)), fill=(255, 0, 0), outline=(255, 0, 0))
        	draw.text((left + 6, bottom - text_height+10), name, fill=(255, 255, 0, 255))            
	elapsedtime=(time.time() - start_time)			
	del draw
	pil_image.save(os.path.join(app.config['STATIC_FOLDER'],'result.jpg'))	
	flash("Query in {:.3f}s".format(elapsedtime))
	return redirect('/result')

@app.route('/registration')
def registration_form():
	return render_template('registration.html',title="Face Image Registration")

@app.route('/registration', methods=['POST'])
def regitration():
	size = (128, 128)
	name=request.form["name"]	
	fn=""
	if request.method == 'POST':
        # check if the post request has the file part
		if 'file' not in request.files:
			flash('No file part')
			return redirect(request.url)
		file = request.files['file']
		if file.filename == '':
			flash('No file selected for uploading')
			return redirect(request.url)
		if file and allowed_file(file.filename):
			filename = secure_filename(file.filename)
			fn=filename
			file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
			db = postgresql.open(app.config['DB_CONN'])			
			image = face_recognition.load_image_file(os.path.join(app.config['UPLOAD_FOLDER'], filename))
			im=Image.open(os.path.join(app.config['UPLOAD_FOLDER'], filename))
			thumbnail=im.resize(size)
			thumbnail.save(os.path.join(app.config['UPLOAD_FOLDER'],'thumbnail.jpg'))			
			data=get_base64_encoded_image(os.path.join(app.config['UPLOAD_FOLDER'],'thumbnail.jpg'))			
			#print(data)			
			encodings = face_recognition.face_encodings(image,num_jitters=5)			
			if len(encodings) > 0:
				query="INSERT INTO encodings (file, name, vector,faceimg) VALUES ('{}', '{}', CUBE(array[{}]), '{}')".format(
			            fn,
			            name,
			            ','.join(str(s) for s in encodings[0][0:128]),
			            data)
				print(db.execute(query))
			flash('{} successfully registered'.format(name))
			return redirect('/registration')
		else:
			flash('Allowed file types are png, jpg, jpeg')
			return redirect(request.url)
			
@app.route('/browse/<int:page>')
def browse_db(page):
	offset=(int(page)-1)*18
	db = postgresql.open(app.config['DB_CONN'])
	query="SELECT COUNT(*) from encodings"
	count=db.query(query)[0][0]	
	query="SELECT id,name,faceimg from encodings ORDER BY name LIMIT 18 OFFSET {};".format(offset)	
	data=db.query(query)	
	#flash("Found {} data records".format(count))
	pagenum=count//18 if count%18==0 else count//18+1		
	pager=pagination(page,pagenum)	
	return render_template('browse.html',
							title="Face Database",
							data=data,
							pagenum=pagenum,
							search=False,
							current=page,
							count=count,
							pager=pager)

@app.route('/search',methods=['POST'])
def search():
	pattern=request.form['search']	
	db = postgresql.open(app.config['DB_CONN'])
	query="SELECT id,name,faceimg from encodings WHERE lower(name) LIKE '%{}%';".format(pattern)
	data=db.query(query)	
	flash("Found {} data records".format(len(data)))
	pagenum=len(data)
	return render_template('browse.html',title="Face Database",data=data,pagenum=pagenum,search=True)

@app.route('/delete/<id>')
def delete_id(id):
	db = postgresql.open(app.config['DB_CONN'])
	query="DELETE from encodings where id={};".format(id)
	data=db.query(query)	
	return redirect('/browse/1')

def gen(camera):
    while True:
        frame = camera.get_frame()
        yield (b'--frame\r\n'
               b'Content-Type: image/png\r\n\r\n' + frame + b'\r\n')	

@app.route("/video_feed")
def video_feed():
    camera.run()
    return Response(gen(camera),mimetype="multipart/x-mixed-replace; boundary=frame")

@app.route("/stream")
def stream_page():
    return render_template("stream.html",title="Live Stream Recognition")

if __name__ == "__main__":
	app.run(host='0.0.0.0',port=8008,)
camera.stop()
