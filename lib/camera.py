import cv2
from app import app
import threading
import time
import face_recognition
import postgresql
import os
thread = None
os.environ["OPENCV_FFMPEG_CAPTURE_OPTIONS"] = "rtsp_transport;udp"

class Camera:
    def __init__(self,fps=20,video_source=0,flip=False):
        self.fps = fps
        self.video_source =  video_source
        if(type(video_source) is int):
            self.camera = cv2.VideoCapture(self.video_source)        
        else:
            self.camera = cv2.VideoCapture(self.video_source,cv2.CAP_FFMPEG)        
        self.max_frames = 5*self.fps
        self.frames = []
        self.isrunning = False
        self.flip=flip
    def run(self):
        global thread        
        if thread is None:
            thread = threading.Thread(target=self._capture_loop)
            print("Starting thread...")
            thread.start()            
        self.isrunning = True
    def _capture_loop(self):
        dt = 1/self.fps
        threshold=0.6
        #print("Observing...")
        processed = True
        db = postgresql.open(app.config['DB_CONN'])
        while self.isrunning:
            v,im = self.camera.read()
            if(self.flip):
                im=cv2.flip(im,1)
            if v:
                if(processed):
                    face_locations = face_recognition.face_locations(im)
                    face_encodings = face_recognition.face_encodings(im, face_locations)
                    face_names = []
                    for face_encoding in face_encodings:                    
                        name = "Unknown"
                        query = "SELECT name FROM encodings WHERE sqrt(power(CUBE(array[{}]) <-> vector, 2)) <= {} ".format(','.join(str(s) for s in face_encoding[0:128]),
                        threshold)+\
                            "ORDER BY sqrt(power(CUBE(array[{}]) <-> vector, 2)) ASC LIMIT 1".format(','.join(str(s) for s in face_encoding[0:128]))
                        q=db.query(query)
                        if(len(q)>0):
                            name=q[0][0]
                        face_names.append(name) 
                processed = not processed                     
                for (top, right, bottom, left), name in zip(face_locations, face_names):   
                    cv2.rectangle(im, (left, top), (right, bottom), (0, 0, 255), 2)
                    cv2.rectangle(im, (left, bottom - 35), (right, bottom), (0, 0, 255), cv2.FILLED)
                    font = cv2.FONT_HERSHEY_SIMPLEX
                    cv2.putText(im, name, (left + 6, bottom - 6), font, 0.8, (255, 255, 255), 1)                    
                if len(self.frames)==self.max_frames:
                    self.frames = self.frames[1:]
                self.frames.append(im)
            time.sleep(dt)
    def stop(self):
        self.camera.release()
        thread.join(timeout=1)
        self.isrunning = False
    def get_frame(self, bytes=True):
        if len(self.frames)>0:
            if bytes:
                img = cv2.imencode('.jpg',self.frames[-1])[1].tobytes()
            else:
                img = self.frames[-1]
        else:
            with open("static/img/not_found.jpeg","rb") as f:
                img = f.read()
        return img
        
