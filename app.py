from flask import Flask

UPLOAD_FOLDER = './uploads'
STATIC_FOLDER = './static'

app = Flask(__name__)
app.secret_key = "secret key"
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['STATIC_FOLDER'] = STATIC_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0
app.config['DB_CONN']="pq://postgres:password@172.17.0.1:5432/face"