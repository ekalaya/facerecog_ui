# Face Recognition UI


This app is a user interface for [Face Recognition](https://github.com/ageitgey/face_recognition)


Features
---
1. Face Registration
2. Face Recognition Test
3. Live Stream Recognition
4. Database browser
5. Face encoding is stored in postgresql
6. Flask Web Framework

Installation
---


Requirements
---
* Python 3.3+
* PostgreSQL with modified cube length. See [this](https://github.com/unoexperto/docker-postgresql-large-cube)

First, make sure you have dlib already installed with Python bindings:

* [How to install dlib from source on macOS or Ubuntu](https://gist.github.com/ageitgey/629d75c1baac34dfa5ca2a1928a7aeaf)

1. Clone this repository 

`git clone https://gitlab.com/ekalaya/facerecog_ui.git && cd facerecog_ui`

2. install requirements

`pip install -r requirements.txt `

3. Modify and run initdb.py

`python initdb.py`

4. Edit app.py. Make sure you have correct db connection

Edit this section in app.py

`app.config['DB_CONN']="pq://postgres:password@172.17.0.1:5432/face"`

4. Run program

`python main.py`

5. open your browser and point to http://localhost:8008


Screenshot
---

![Screenshot](recognition.png)

![Screenshot](result.png)

![Screenshot](live.png)




