import postgresql

#change user pass host port for postgresql connection
def setup_db():
    db = postgresql.open('pq://user:pass@localhost:5432/face')
    db.execute("create extension if not exists cube;")
    db.execute("drop table if exists encodings")
    db.execute("create table encodings (id serial, file varchar, name varchar, faceimg text, vector cube);")
    db.execute("create index encodings_idx on encodings (vector);")

setup_db()
